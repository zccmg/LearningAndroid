import java.util.Objects;

public class Products {

    private String name;
    private Double price;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product Name:" + this.name + " Price:" + this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
