import java.util.ArrayList;

public interface ItemsInterface {
    void AddItem(Items items);
    void RemoveItem(Items items);
    ArrayList<Items> getAll();
}
